import Game from "./my2048";

const menu = document.getElementsByClassName("choice");
const newGame = new Game();
for (const btn of menu) {
  btn.onclick = async function () {
    document.getElementById("gameOver").innerText = "";
    let map = document.getElementsByClassName("map")[0];
    if (typeof map !== "undefined") {
      map.parentNode.removeChild(map);
    }
    if (btn.id === "classique") {
      newGame.restart(4, 4);
    }
    if (btn.id === "submit-custom") {
      let customNbRow = document.getElementById("nb-row").value;
      let customNbCol = document.getElementById("nb-col").value;
      if (
        12 >= parseInt(customNbCol) &&
        parseInt(customNbCol) >= 2 &&
        12 >= parseInt(customNbRow) &&
        parseInt(customNbRow) >= 2
      ) {
        newGame.restart(parseInt(customNbRow), parseInt(customNbCol));
        document.getElementById("nb-row").value = "";
        document.getElementById("nb-col").value = "";
      } else {
        alert(
          "Veuillez choisir des nombres de rangées et de colonnes compris entre 2 et 12"
        );
        console.log("Nop");
      }
    }
    if (btn.id === "prefer") {
      let joketable = document.createElement("div");
      joketable.classList.add("map", "col-xs-12");
      let row = document.createElement("div");
      let cell = document.createElement("div");
      cell.innerText = "2048";
      let titleJoke = document.createElement("h1");
      titleJoke.innerText = "Bravo!!!";
      let bodyJoke = document.createElement("p");
      bodyJoke.innerText =
        "Je gagne souvent à celui la c'est pour ca que je l'aime !o)";

      row.append(cell);
      joketable.append(row);
      joketable.append(titleJoke);
      joketable.append(bodyJoke);
      document.getElementById("games").append(joketable);
    }
  };
}
