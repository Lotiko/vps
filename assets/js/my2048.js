/**
 * My 2048 game you can make a gameboard with size you want
 *
 * @export
 * @class My2048
 */
export default class My2048 {
  constructor() {
    this.currentTab = [];
    this.lineForCompute = [];
    this.score = 0;
    this.nbRows = 0;
    this.nbCols = 0;
    this.reference;
  }
  /**
   * Creates a "keyup" event, if there is already one it is destroyed before being recreated.
   *
   * @memberof My2048
   */
  createEvent() {
    /// remove event if change type of game without the player has lose
    document.removeEventListener("keyup", this.reference)
      ? document.removeEventListener("keyup", this.reference)
      : "";
    const that = this;
    document.addEventListener(
      "keyup",
      (this.reference = function (ev) {
        that.start(ev);
      })
    );
  }

  /**
   * Creates a 2048 game, set properties of the class, call methodes to render the first gameboard.
   *
   * @param {Number} nbRows
   * @param {Number} nbCols
   * @memberof My2048
   */
  restart(nbRows, nbCols) {
    //// destroy the element gameOver, otherwise it's awkward to insert the game board
    let oldturn = document.getElementById("gameOver");
    oldturn.innerHTML = "";

    this.createEvent();
    this.nbCols = nbCols;
    this.nbRows = nbRows;
    this.score = 0;
    this.createTab();
    this.generateCell();
    this.createHtml(this.currentTab);
  }
  ///////////////////functions to create game
  /**
   * Create an tab with only zero values
   *
   * @returns
   * @memberof My2048
   */
  createTab() {
    let tab = [];
    let line = [];
    for (let r = 0; r < this.nbRows; r++) {
      for (let c = 0; c < this.nbCols; c++) {
        line[c] = 0;
      }
      tab[r] = line;
      line = [];
    }
    this.currentTab = tab;
  }

  /**
   * to choose one cell of the tab and generate random val (90% chance to have 2 else 4)
   * @returns {{val: number, col: number, line: number}}
   */
  random1() {
    let r0_ = Math.floor(Math.random() * this.nbRows + 1);
    let r_0 = Math.floor(Math.random() * this.nbCols + 1);
    let chance = Math.random() * 100;
    let valCell;
    if (chance < 90) {
      valCell = 2;
    } else {
      valCell = 4;
    }
    console.log(valCell);
    return { line: r0_, col: r_0, val: valCell };
  }

  /**
   *
   * @param t {[currentTab]}
   * @param nb number of cell to generate
   * @returns {Promise<[currentTab]>}
   */
  generateCell() {
    let cell1 = this.random1();
    let cell2 = this.random1();
    while (cell2.line === cell1.line && cell2.col === cell1.col) {
      cell2 = this.random1();
    }
    this.currentTab[cell1.line - 1][cell1.col - 1] = cell1.val;
    this.currentTab[cell2.line - 1][cell2.col - 1] = cell2.val;
  }
  /**
   * create the html of gameBoard and score
   * @param tab
   * @returns {HTMLDivElement}
   */
  createHtml() {
    let newGameBoard = document.createElement("div");
    newGameBoard.classList.add("map", "col-xs-12");
    let r = 1;
    let c = 1;
    for (let line of this.currentTab) {
      let row = document.createElement("div");
      row.classList.add("r-" + r, "col-xs-" + this.nbCols);
      for (let val of line) {
        let cell = document.createElement("div");
        cell.classList.add("cell", "r" + r + "-c" + c);
        cell.innerText = val;
        if (val === 0) {
          cell.setAttribute("style", "color: transparent;");
        }
        row.append(cell);
        c++;
      }
      newGameBoard.append(row);
      c = 1;
      r++;
    }
    let divScore = document.createElement("div");
    divScore.classList.add("score");
    divScore.innerHTML = `<p>Score : ${this.score}</p>`;
    newGameBoard.append(divScore);
    document.getElementById("games").append(newGameBoard);
  }
  /////////////////////////////////////////////////
  //////s for move
  /**
   * Process of move in currentTab return true if has move, false else
   * @param direction
   * @returns {Promise<boolean>}
   */
  goMove(direction) {
    let verifyMove = false;
    let where = "before";
    if (direction === "down" || direction === "up") {
      this.createColTab();
    }
    if (direction === "left" || direction === "up") {
      where = "after";
    }
    const sizeOfLine = this.currentTab[0].length;
    for (let line = 0; line < this.currentTab.length; line++) {
      this.lineForCompute = [];
      this.removeZero(line, sizeOfLine);
      this.fusion(direction);
      this.insertZero(where, sizeOfLine);
      for (const [i, v] of this.lineForCompute.entries()) {
        if (this.currentTab[line][i] !== v) {
          verifyMove = true;
        }
      }
      this.currentTab[line] = this.lineForCompute;
    }
    if (direction === "down" || direction === "up") {
      this.createColTab();
    }
    return verifyMove;
  }

  /**
   * transform column in line of currentTab
   * @returns {[currentTab]}
   */
  createColTab() {
    let tabCol = [];
    let colToLine = [];
    for (let c = 0; c < this.currentTab[0].length; c++) {
      for (let l = 0; l < this.currentTab.length; l++) {
        colToLine.push(this.currentTab[l][c]);
      }
      tabCol.push(colToLine);
      colToLine = [];
    }
    this.currentTab = tabCol;
  }

  /**
   * remove zero of tab line
   * @param t {currentTab[]} Line in move()
   * @returns {[]}
   */
  removeZero(nbLine, length) {
    for (let i = 0; i < length; i++) {
      if (this.currentTab[nbLine][i] !== 0) {
        this.lineForCompute.push(this.currentTab[nbLine][i]);
      }
    }
  }

  /**
   * if two equals cell in direction move, remove one cell and second = value * 2
   * @param this.lineForCompue {[]} newTabLine in move()
   * @param direction {string} left,right,up or down
   * @returns {[]}
   */
  fusion(direction) {
    let fusionLine = [];
    if (this.lineForCompute.length === 1) {
      return this.lineForCompute;
    }
    if (direction === "down" || direction === "right") {
      for (let l = this.lineForCompute.length - 1; l >= 0; l--) {
        if (this.lineForCompute[l] === this.lineForCompute[l - 1]) {
          fusionLine.unshift(this.lineForCompute[l] * 2);
          this.score = this.score + this.lineForCompute[l] * 2;
          if (this.lineForCompute[l] * 2 === 2048) {
            this.win();
          }
          l--;
        } else {
          fusionLine.unshift(this.lineForCompute[l]);
        }
      }
    } else {
      for (let l = 0; l <= this.lineForCompute.length - 1; l++) {
        if (this.lineForCompute[l] === this.lineForCompute[l + 1]) {
          fusionLine.push(this.lineForCompute[l] * 2);
          this.score = this.score + this.lineForCompute[l] * 2;
          if (this.lineForCompute[l] * 2 === 2048) {
            this.win();
          }
          l++;
        } else {
          fusionLine.push(this.lineForCompute[l]);
        }
      }
    }
    this.lineForCompute = fusionLine;
  }

  /**
   * insert zero before or after value in tab for good length
   * @param line {[]} newTabLine in move()
   * @param where {string} before or after
   * @param size {number} nbRows for up and down, nbCols for left and right
   * @returns {[]}
   */
  insertZero(where, size) {
    while (this.lineForCompute.length < size) {
      if (where === "before") {
        this.lineForCompute.unshift(0);
      } else {
        this.lineForCompute.push(0);
      }
    }
  }
  ////////////////////////////////
  ////////////// process game

  /**
   * to catch key value and insert new gameboard after move or stop game when lose
   * @param event
   * @returns {Promise<void>}
   */
  start(event) {
    let map = document.getElementsByClassName("map")[0];
    let hasMove = false;
    if (typeof map !== "undefined") {
      map.parentNode.removeChild(map);
      switch (event.key) {
        case "ArrowRight":
          hasMove = this.goMove("right");
          break;
        case "ArrowLeft":
          hasMove = this.goMove("left");
          break;
        case "ArrowUp":
          hasMove = this.goMove("up");
          break;
        case "ArrowDown":
          hasMove = this.goMove("down");
          break;
        default:
          break;
      }
      if (hasMove !== false) {
        this.insertNewCell();
        this.createHtml();
        return;
      } else {
        if (this.hasLose() === true) {
          this.gameOver();
        } else {
          this.createHtml();
        }
      }
    } else {
      return;
    }
  }
  /**
   * insert new val in random empty cell for each turn
   * @param oldTab
   * @returns {Promise<void>}
   */
  insertNewCell() {
    let tab = [];
    for (let r = 0; r < this.nbRows; r++) {
      for (let c = 0; c < this.nbCols; c++) {
        if (this.currentTab[r][c] === 0)
          tab.push({ rowOfCell: r, colOfCell: c });
      }
    }
    if (tab.length === 0) {
      return;
    }
    let newCell = this.random1();
    let choseCell = Math.floor(Math.random() * tab.length);
    this.currentTab[tab[choseCell].rowOfCell][tab[choseCell].colOfCell] =
      newCell.val;
  }

  /**
   * check if you can play
   * @returns {boolean}
   */
  hasLose() {
    let lose = true;
    for (let l = 0; l < this.nbRows; l++) {
      for (let c = 0; c < this.nbCols; c++) {
        if (this.currentTab[l][c] === 0) {
          lose = false;
          break;
        } else if (c === this.nbCols - 1 && l !== this.nbRows - 1) {
          if (this.currentTab[l][c] === this.currentTab[l + 1][c]) {
            lose = false;
            break;
          }
        } else if (l === this.nbRows - 1) {
          if (this.currentTab[l][c] === this.currentTab[l][c + 1]) {
            lose = false;
            break;
          }
        } else {
          if (
            this.currentTab[l][c] === this.currentTab[l + 1][c] ||
            this.currentTab[l][c] === this.currentTab[l][c + 1]
          ) {
            lose = false;
            break;
          }
        }
      }
      if (lose !== true) {
        break;
      }
    }
    return lose;
  }

  /**
   * insert message when you lose with score and remove eventListener(for replay)
   */
  gameOver() {
    let gameOver = document.getElementById("gameOver");
    if (gameOver.innerText === "") {
      let messH = document.createElement("h1");
      let messP = document.createElement("p");
      messH.textContent = "Game Over!!";
      messP.textContent = "Vous avez perdu votre score est de : " + this.score;
      gameOver.append(messH);
      gameOver.append(messP);
      document.removeEventListener("keyup", this.reference);
    }
  }
  /**
   * insert message when you win with score
   */
  win() {
    let texte = `BRAVO!!!\nVous avez gagner avec un score de ${this.score}`;
    alert(texte);
  }
}
