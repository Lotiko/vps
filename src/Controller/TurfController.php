<?php

namespace App\Controller;

use App\Entity\RestAdapt;
use App\Entity\Quinte;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TurfController extends AbstractController
{

    private  $_instanceQuinte;
    private  $_instanceRestAdapt;
    private $entityManager;

    private function getInstances()
    {

        $this->entityManager = $this->getDoctrine()->getManager();
        $this->_instanceQuinte = $this->entityManager->getRepository(Quinte::class)->last();
        $this->_instanceRestAdapt = $this->entityManager->getRepository(RestAdapt::class)->findOneBy(['name' => 'pmu']);
    }

    public static array $participantsData;

    /**
     * @Route("/turf", name="turf")
     * @return Response
     */
    public function index()
    {
        $this->getInstances();

        $stateQuinte = $this->_instanceRestAdapt->fetchDataRest("/rest/client/1/evenements/quinte");
        $quinte = new Quinte();
        $quinte->setByRestAdapt($stateQuinte, $this->_instanceRestAdapt);
        $urlRace = "/rest/client/1/programme/" . str_replace("-", "", $quinte->getDay()) . "/R" . $quinte->getMeeting() . "/C" . $quinte->getRace();
        $quinteData = $this->_instanceRestAdapt
            ->fetchDataRest($urlRace);
        $quinte->pushInQuinte($quinteData);
        // dump($quinteData);
        if ($this->_instanceQuinte == null || $this->_instanceQuinte->getDay() !== $quinte->getDay()) {
            // dump($quinte);
            // exit;
            $this->entityManager->persist($quinte);
            $this->entityManager->flush();
        }
        $participantsData = $this->_instanceRestAdapt->fetchDataRest($urlRace . "/participants");
        $body = $quinte->sortTabQuinte($participantsData, $quinte->getDiscipline());
        // dump(['head' => $this->entityManager->getRepository(Quinte::class)->last(), 'body' => $body, 'page' => ' Le quinte !']);
        // exit;
        return $this->render('turf/index.html.twig', ['head' => $this->entityManager->getRepository(Quinte::class)->last(), 'body' => $body, 'page' => ' Le quinte !']);
    }

    /**
     * @Route("/turf/prono/{meeting}/{race}/{day}", name="turfperfs")
     * @return Response
     */
    public function perfs()
    {
        $this->getInstances();
        $urlPerfs = "/rest/client/1/programme/" . str_replace("-", "", $this->_instanceQuinte->getDay()) . "/R" . $this->_instanceQuinte->getMeeting() . "/C" . $this->_instanceQuinte->getRace() . "/performances-detaillees/pretty";
        $participantsPerf = $this->_instanceRestAdapt->fetchDataRest($urlPerfs);
        $dataPerfs = $this->_instanceQuinte->sortPerfs($participantsPerf);
        return $this->render('turf/perfs.html.twig', ['head' => $this->_instanceQuinte->getAll(), 'body' => $dataPerfs, 'page' => ' Les Perfs !']);
    }
    /**
     * @Route("/turf/dev", name="turfdev")
     */
    public function dev()
    {
        //return new Response(json_encode($adaptRepository->findOneBy(["name" => "pmu"])));
        $entityManager = $this->getDoctrine()->getManager();
        $adapt = new RestAdapt();
        $adapt->setName('pmu');
        $adapt->setUrl('https://online.turfinfo.api.pmu.fr');
        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($adapt);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response('Saved new restadapt with id ' . $adapt->getId() . '  ' . $adapt->getName());
    }
}
