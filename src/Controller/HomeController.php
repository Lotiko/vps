<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function goHome()
    {
        return $this->redirect('/home');
    }

    /**
     * @Route("/home")
     */
    public function home()
    {
        $test = "test";
        return $this->render('pages/home.html.twig', ['page' => 'Welcome!']);
    }
    /**
     * @Route("/my2048")
     */
    public function my2048()
    {
        return $this->render('pages/my2048.html.twig', ['page' => ' Lo Jeu !']);
    }

    /**
     * @Route("/cv")
     */
    public function index()
    {
        return $this->render('pages/cv.html.twig', ["page" => " C'est qui ?"]);
    }
    /**
     * @Route("/info")
     */
    // public function info()
    // {
    //     phpinfo();
    // }
}
