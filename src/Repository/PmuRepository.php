<?php

namespace App\Repository;

use App\Entity\Pmu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pmu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pmu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pmu[]    findAll()
 * @method Pmu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PmuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pmu::class);
    }

    /**
     * @return Pmu|null
     */
    public function findLast(): ?Pmu
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT p
            FROM App\Entity\Pmu p
            ORDER BY p.id desc'
        );


        // returns an array of Product objects
        // dump($query->getResult());
        // exit;
        return $query->getResult() ? $query->getResult()[0] : null;
    }
}
