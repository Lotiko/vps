<?php

namespace App\Repository;

use App\Entity\RestAdapt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RestAdapt|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestAdapt|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestAdapt[]    findAll()
 * @method RestAdapt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestAdaptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RestAdapt::class);
    }

    // /**
    //  * @return RestAdapt[] Returns an array of RestAdapt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    // public function findOneByName($value): ?RestAdapt
    // {
    //     return $this->createQueryBuilder('r')
    //         ->andWhere('r.name = :val')
    //         ->setParameter('val', $value)
    //         ->getQuery()
    //         ->getOneOrNullResult()
    //     ;
    // }
}
