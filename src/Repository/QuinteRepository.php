<?php

namespace App\Repository;

use App\Entity\Quinte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Quinte|null find($id, $lockMode = null, $lockVersion = null)
 * @method Quinte|null findOneBy(array $criteria, array $orderBy = null)
 * @method Quinte[]    findAll()
 * @method Quinte[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuinteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Quinte::class);
    }

    // /**
    //  * @return Quinte[] Returns an array of Quinte objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Quinte
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * return the last record of quinte
     * @return Quinte|null
     */
    public function last(): ?Quinte
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT p
            FROM App\Entity\Quinte p
            ORDER BY p.id DESC'
        )->setMaxResults(1);
        return $query->getResult() ? $query->getResult()[0] : null;
    }
}
