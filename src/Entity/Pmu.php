<?php

namespace App\Entity;

use DateTime;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\PmuRepository::class)
 * @UniqueEntity("name")
 * @UniqueEntity("url")
 */
class Pmu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(name="name",type="string", length=255, unique=true)
     * @Assert\Unique
     */
    private ?string $name;

    /**
     * @ORM\Column(name="url", type="string", length=255, unique=true)
     * @Assert\Unique
     */
    private ?string $url;

    /**
     * @ORM\Column(type="object")
     */
    private $quinte;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getQuinte()
    {
        return $this->quinte;
    }

    public function setQuinte($quinte): self
    {
        $this->quinte = $quinte;

        return $this;
    }
    public function getDay()
    {
        return $this->quinte['day'];
    }

    /**
     * @param $dataRest
     * @param RestAdapt $restAdapt
     */
    public function setByRestAdapt($dataRest, RestAdapt $restAdapt)
    {
        $date = $this->timestampToDateHour($dataRest['quinte']['heureDepart']);
        $this->setQuinte([
            'day' => $date['day'],
            'hour' => $date['hour'],
            'meeting' => $dataRest['quinte']['numeroInterneReunion'],
            'race' => $dataRest['quinte']['numeroCourse'],
            'place' => $dataRest['quinte']['nomHippodrome'],
            'name' => $dataRest['quinte']['libelleCourse'],
            'discipline' => $dataRest['quinte']['discipline']
        ]);
        $this->setName($dataRest['quinte']['libelleCourse'] . "/" . $date['day']);
        $this->setUrl($restAdapt->getUrl() . $date['day']);
    }
    public function timestampToDateHour($timestamp)
    {
        $datetimeFormat = 'd-m-Y H\Hi';
        try {
            $date = new DateTime('now', new DateTimeZone('Indian/Reunion'));
        } catch (Exception $e) {
            return [
                'day' => 'no date',
                'hour' => 'no hour'
            ];
        }
        $date->setTimestamp($timestamp / 1000);
        $strDate = $date->format($datetimeFormat);
        return [
            'day' => substr($strDate, 0, 10),
            'hour' => substr($strDate, 11)
        ];
    }

    public function sortQuinte(array $dataRest, string $discipline): array
    {
        //        dump($dataRest);exit;
        $participants = [];
        if ($discipline === "ATTELE") {
            foreach ($dataRest['participants'] as $v) {
                $participants += [$v['numPmu'] =>
                [
                    "name" => isset($v['nom']) ? $v['nom'] : "",
                    "age" => isset($v['age']) ? $v['age'] : "",
                    "sexe" => isset($v['sexe']) ? $v['sexe'] : "",
                    "status" => isset($v['statut']) ? $v['statut'] : "",
                    "owner" => isset($v['proprietaire']) ? $v['proprietaire'] : "",
                    "coach" => isset($v['entraineur']) ? $v['entraineur'] : "",
                    "deferre" => isset($v['deferre']) ? $v['deferre'] : "",
                    "driver" => isset($v['driver']) ? $v['driver'] : "",
                    "musique" => isset($v['musique']) ? $v['musique'] : "",
                    "money" => isset($v['gainsParticipant']['gainsCarriere']) ? $v['gainsParticipant']['gainsCarriere'] : "",
                    "distance" => isset($v['handicapDistance']) ? $v['handicapDistance'] : "",
                    "rapport" => isset($v['dernierRapportDirect']['rapport']) ? $v['dernierRapportDirect']['rapport'] : "",
                    "logoCasaque" => isset($v['urlCasaque']) ? $v['urlCasaque'] : ""
                ]];
            }
        }
        if ($discipline === "PLAT") {
            foreach ($dataRest['participants'] as $v) {
                $participants += [$v['numPmu'] =>
                [
                    "name" => isset($v['nom']) ? $v['nom'] : "",
                    "age" => isset($v['age']) ? $v['age'] : "",
                    "sexe" => isset($v['sexe']) ? $v['sexe'] : "",
                    "status" => isset($v['statut']) ? $v['statut'] : "",
                    "owner" => isset($v['proprietaire']) ? $v['proprietaire'] : "",
                    "coach" => isset($v['entraineur']) ? $v['entraineur'] : "",
                    "corde" => isset($v['placeCorde']) ? $v['placeCorde'] : "",
                    "driver" => isset($v['driver']) ? $v['driver'] : "",
                    "musique" => isset($v['musique']) ? $v['musique'] : "",
                    "money" => isset($v['gainsParticipant']['gainsCarriere']) ? $v['gainsParticipant']['gainsCarriere'] / 100 : "",
                    "valeur" => isset($v['handicapValeur']) ? $v['handicapValeur'] : "",
                    "poids" => isset($v['handicapPoids']) ? $v['handicapPoids'] : "",
                    "rapport" => isset($v['dernierRapportDirect']['rapport']) ? $v['dernierRapportDirect']['rapport'] : "",
                    "logoCasaque" => isset($v['urlCasaque']) ? $v['urlCasaque'] : ""
                ]];
            }
        }
        return $participants;
    }

    public function pushInQuinte(array $dataTopush)
    {
        $newQuinte = $this->getQuinte();
        $newQuinte += $dataTopush;
        $this->setQuinte($newQuinte);
    }
    public function sortPerf(array $dataPerfs)
    {
        //        dump($dataPerfs);exit;
        $retPerf = [];
        foreach ($dataPerfs['participants'] as $key => $vPerf) {
            $detailsPerf = $vPerf['coursesCourues'];
            $retPerf += [$vPerf['numPmu'] => ['name' => $vPerf['nomCheval']]];
            //            dump($retPerf);exit;

            foreach ($detailsPerf as $vDetail) {
                //                dump($vDetail);exit;
                $perf = [
                    "date" => $this->timestampToDateHour($vDetail['date'])['day'],
                    "hippodrome" => isset($vDetail['hippodrome']) ? $vDetail['hippodrome'] : "",
                    "discipline" => isset($vDetail['discipline']) ? $vDetail['discipline'] : "",
                    "allocation" => isset($vDetail['allocation']) ? $vDetail['allocation'] : "",
                    "distance" => isset($vDetail['distance']) ? $vDetail['distance'] : "",
                    "nbParticipants" => isset($vDetail['nbParticipants']) ? $vDetail['nbParticipants'] : "",
                    //"tempsDuPremier" => 10170
                    "terrain" => isset($vDetail['etatTerrain']) ? $vDetail['etatTerrain'] : "",
                    "participants" => isset($vDetail['participants']) ? $vDetail['participants'] : ""
                ];
                $retPerf[$vPerf['numPmu']][] = $perf;
            }
        }
        //            dump($retPerf); exit;
        return $retPerf;
    }
}
