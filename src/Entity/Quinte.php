<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use DateTimeZone;
use Exception;

/**
 * @ORM\Entity(repositoryClass=App\Repository\QuinteRepository::class)
 */
class Quinte
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $day;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $hour;

    /**
     * @ORM\Column(type="integer")
     */
    private $meeting;

    /**
     * @ORM\Column(type="integer")
     */
    private $race;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $place;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $discipline;

    /**
     * @ORM\Column(type="integer")
     */
    private $cashPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $distance;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $distanceUnit;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $corde;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $specialite;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $conditions;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    public function getHour(): ?string
    {
        return $this->hour;
    }

    public function setHour(string $hour): self
    {
        $this->hour = $hour;

        return $this;
    }

    public function getMeeting(): ?int
    {
        return $this->meeting;
    }

    public function setMeeting(int $meeting): self
    {
        $this->meeting = $meeting;

        return $this;
    }

    public function getRace(): ?int
    {
        return $this->race;
    }

    public function setRace(int $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDiscipline(): ?string
    {
        return $this->discipline;
    }

    public function setDiscipline(string $discipline): self
    {
        $this->discipline = $discipline;

        return $this;
    }
    public function getAll()
    {
        return [
            "id" => $this->getId(),
            "day" => $this->getDay(),
            "hour" => $this->getHour(),
            "meeting" => $this->getMeeting(),
            "race" => $this->getRace(),
            "place" => $this->getPlace(),
            "name" => $this->getName(),
            "discipline" => $this->getDiscipline(),
            "cashprice" => $this->getCashPrice(),
            "distance" => $this->getDistance(),
            "distanceUnit" => $this->getDistanceUnit(),
            "corde" => $this->getCorde(),
            "specialite" => $this->getSpecialite(),
            "conditions" => $this->getConditions(),

        ];
    }
    public function timestampToDateHour($timestamp)
    {
        $datetimeFormat = 'd-m-Y H\Hi';
        try {
            $date = new DateTime('now', new DateTimeZone('Indian/Reunion'));
        } catch (Exception $e) {
            return [
                'day' => 'no date',
                'hour' => 'no hour'
            ];
        }
        $date->setTimestamp($timestamp / 1000);
        $strDate = $date->format($datetimeFormat);
        return [
            'day' => substr($strDate, 0, 10),
            'hour' => substr($strDate, 11)
        ];
    }
    /**
     * @param $dataRest
     * @param RestAdapt $restAdapt
     */
    public function setByRestAdapt($dataRest)
    {
        $date = $this->timestampToDateHour($dataRest['quinte']['heureDepart']);
        $this->setDay($date['day']);
        $this->setHour($date['hour']);
        $this->setMeeting($dataRest['quinte']['numeroInterneReunion']);
        $this->setRace($dataRest['quinte']['numeroCourse']);
        $this->setPlace($dataRest['quinte']['nomHippodrome']);
        $this->setName($dataRest['quinte']['libelleCourse']);
        $this->setDiscipline($dataRest['quinte']['discipline']);
    }
    public function pushInQuinte(array $data)
    {
        $this->setCashPrice($data['montantPrix']);
        $this->setDistance($data['distance']);
        $this->setDistanceUnit($data['distanceUnit']);
        $this->setDiscipline($data['discipline']);
        $this->setSpecialite($data['specialite']);
        $this->setConditions($data['conditions']);
    }
    public function sortTabQuinte(array $dataRest, string $discipline): array
    {
        // dump($dataRest, $discipline);
        // exit;
        $participants = [];
        foreach ($dataRest['participants'] as $v) {
            $participants += [$v['numPmu'] =>
            [
                "name" => isset($v['nom']) ? $v['nom'] : "",
                "age" => isset($v['age']) ? $v['age'] : "",
                "sexe" => isset($v['sexe']) ? $v['sexe'] : "",
                "status" => isset($v['statut']) ? $v['statut'] : "",
                "owner" => isset($v['proprietaire']) ? $v['proprietaire'] : "",
                "coach" => isset($v['entraineur']) ? $v['entraineur'] : "",
                "driver" => isset($v['driver']) ? $v['driver'] : "",
                "musique" => isset($v['musique']) ? $v['musique'] : "",
                "money" => isset($v['gainsParticipant']['gainsCarriere']) ? $v['gainsParticipant']['gainsCarriere'] / 100 : "",
                "rapport" => isset($v['dernierRapportDirect']['rapport']) ? $v['dernierRapportDirect']['rapport'] : "",
                "logoCasaque" => isset($v['urlCasaque']) ? $v['urlCasaque'] : ""
            ]];
            if ($discipline === "ATTELE") {
                // dump('test');
                $participants[$v['numPmu']] +=
                    [
                        "deferre" => isset($v['deferre']) ? $v['deferre'] : "",
                        "distance" => isset($v['handicapDistance']) ? $v['handicapDistance'] : "",
                    ];
            }
            if ($discipline === "PLAT" || $discipline === "HAIES") {
                $participants[$v['numPmu']] +=
                    [
                        "corde" => isset($v['placeCorde']) ? $v['placeCorde'] : "",
                        "valeur" => isset($v['handicapValeur']) ? $v['handicapValeur'] : "",
                        "poids" => isset($v['handicapPoids']) ? $v['handicapPoids'] : "",

                    ];
            }
        }
        // dump($participants);
        // exit;
        return $participants;
    }
    public function sortPerfs(array $dataPerfs)
    {
        //        dump($dataPerfs);exit;
        $retPerf = [];
        foreach ($dataPerfs['participants'] as $key => $vPerf) {
            $detailsPerf = $vPerf['coursesCourues'];
            $retPerf += [$vPerf['numPmu'] => ['name' => $vPerf['nomCheval']]];
            //            dump($retPerf);exit;

            foreach ($detailsPerf as $vDetail) {
                //                dump($vDetail);exit;
                $perf = [
                    "date" => $this->timestampToDateHour($vDetail['date'])['day'],
                    "hippodrome" => isset($vDetail['hippodrome']) ? $vDetail['hippodrome'] : "",
                    "discipline" => isset($vDetail['discipline']) ? $vDetail['discipline'] : "",
                    "allocation" => isset($vDetail['allocation']) ? $vDetail['allocation'] : "",
                    "distance" => isset($vDetail['distance']) ? $vDetail['distance'] : "",
                    "nbParticipants" => isset($vDetail['nbParticipants']) ? $vDetail['nbParticipants'] : "",
                    //"tempsDuPremier" => 10170
                    "terrain" => isset($vDetail['etatTerrain']) ? $vDetail['etatTerrain'] : "",
                    "participants" => isset($vDetail['participants']) ? $vDetail['participants'] : ""
                ];
                $retPerf[$vPerf['numPmu']][] = $perf;
            }
        }
        // dump($retPerf);
        // exit;
        return $retPerf;
    }
    public function getCashPrice(): ?int
    {
        return $this->cashPrice;
    }

    public function setCashPrice(int $cashPrice): self
    {
        $this->cashPrice = $cashPrice;

        return $this;
    }

    public function getDistance(): ?int
    {
        return $this->distance;
    }

    public function setDistance(int $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getDistanceUnit(): ?string
    {
        return $this->distanceUnit;
    }

    public function setDistanceUnit(string $distanceUnit): self
    {
        $this->distanceUnit = $distanceUnit;

        return $this;
    }

    public function getCorde(): ?string
    {
        return $this->corde;
    }

    public function setCorde(?string $corde): self
    {
        $this->corde = $corde;

        return $this;
    }

    public function getSpecialite(): ?string
    {
        return $this->specialite;
    }

    public function setSpecialite(string $specialite): self
    {
        $this->specialite = $specialite;

        return $this;
    }

    public function getConditions(): ?string
    {
        return $this->conditions;
    }

    public function setConditions(?string $conditions): self
    {
        $this->conditions = $conditions;

        return $this;
    }
}
