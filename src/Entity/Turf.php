<?php

namespace App\Entity;


use Symfony\Component\HttpClient\HttpClient;

use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


class Turf
{
    private $client;
    private $dataByNumber = [];
    private $perfByNumber = [];
    private $quinte;
    private $dayForUrl;


    public function __construct()
    {
        $this->client = HttpClient::create();
    }

    public function fetchQuinte(): array
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://online.turfinfo.api.pmu.fr/rest/client/1/evenements/quinte'
            );
            $content = $response->getContent();
//            var_dump($response->toArray());exit;
            $headData =  $this->sortQuinte($response->toArray());
//            $this->dayForUrl = str_replace("-", "", $headData['day']);
            $this->infoParticipants($this->dayForUrl, $headData['meeting'], $headData['race']);
            $this->myProno($this->dayForUrl, $headData['meeting'], $headData['race']);
            var_dump(["head" => $headData, "body" => $this->dataByNumber]);exit;
//            return ["head" => $headData, "body" => $this->dataByNumber];
        } catch (TransportExceptionInterface $e) {
            $eMessage = "Probléme avec la source de données.";
        }
    }

    private function sortQuinte($dataPmu)
    {
        $this->quinte = $this->timestampToDateHour($dataPmu['quinte']['heureDepart']/1000, $dataPmu['quinte']['timezoneOffset']);
        $this->dayForUrl = str_replace("-", "", $this->quinte['day']);
        $this->quinte += [
            'meeting' => $dataPmu['quinte']['numeroInterneReunion'],
            'race' => $dataPmu['quinte']['numeroCourse'],
            'place' => $dataPmu['quinte']['nomHippodrome'],
            'name' => $dataPmu['quinte']['libelleCourse'],
            'type' => $dataPmu['quinte']['discipline']];
        try {
//        var_dump($this->dayForUrl);exit;
            $response = $this->client->request(
                'GET',
                'https://online.turfinfo.api.pmu.fr/rest/client/1/programme/'.$this->dayForUrl.'/R'.$this->quinte['meeting'].'/C'.$this->quinte['race']
            );

//            var_dump($response->toArray());exit;
            $datarace = $response->toArray();
            $this->quinte += array_slice($datarace, 11, 20);
            var_dump($this->quinte);exit;
        } catch (TransportExceptionInterface $e) {
            $eMessage = "Probléme avec la source de données.";
        }
        return $this->quinte;

    }

    private function timestampToDateHour($timestamp, $timezone)
    {
        $datetimeFormat = 'd-m-Y H\Hi';
        $date = new \DateTime();
        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $date->setTimestamp($timestamp);
        $strDate = $date->format($datetimeFormat);
        return ['day' => substr($strDate,0,10),
            'hour' => substr($strDate,11)];
    }

    private function infoParticipants(string $day, int $meeting, int $race) : void
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://online.turfinfo.api.pmu.fr/rest/client/1/programme/'.$day.'/R'.$meeting.'/C'.$race.'/participants'
            );
            $statusCode = $response->getStatusCode();
            $contentType = $response->getHeaders()['content-type'][0];
            $content = $response->getContent();
//            var_dump($response->toArray());exit;
            $this->dataByNumber = $response->toArray()['participants'];
        } catch (TransportExceptionInterface $e) {
            $eMessage = "Probléme avec la source de données.";
        }
    }

    private function myProno(string $day, int $meeting, int $race) :void
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://online.turfinfo.api.pmu.fr/rest/client/1/programme/'.$day.'/R'.$meeting.'/C'.$race.'/performances-detaillees/pretty'
            );
            $statusCode = $response->getStatusCode();
            $contentType = $response->getHeaders()['content-type'][0];
            $content = $response->getContent();
            /*
             * var_dump($perfData['participants'][0]['coursesCourues'][0]['participants']);exit;
             *  0 => TODO foreach participants + coursecourue trouvé algo compare
              *      array (size=6)
               *       'place' =>
                *        array (size=2)
                 *         'place' => int 1
                  *        'statusArrivee' => string 'PLACE' (length=5)
                   *   'nomCheval' => string 'VICTOR FERM' (length=11)
                    *  'nomJockey' => string 'A. GUZZINATI' (length=12)
                     * 'itsHim' => boolean false
                      *'reductionKilometrique' => int 7300
                      *'distanceParcourue' => int 2150
             */
            $perfData =  $response->toArray();
            array_map([$this, 'sortDataForProno'], $perfData['participants']);
//            var_dump($this->dataByNumber);exit;
//            var_dump($perfData['participants']);exit;
//            return [];
        } catch (TransportExceptionInterface $e) {
            $eMessage = "Probléme avec la source de données.";
        }
    }

    private function sortDataForProno($participant)
    {
        if ($this->quinte['type'] === 'ATTELE') {

            $this->perfByNumber['current'][] = $participant;
        }
    }
}
