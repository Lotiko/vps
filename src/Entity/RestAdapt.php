<?php

namespace App\Entity;

use App\Repository\RestAdaptRepository;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * @ORM\Entity(repositoryClass=RestAdaptRepository::class)
 */
class RestAdapt
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\Unique
     */
    private $name;

    /**
     * @ORM\Column(name="url", type="string", length=255, unique=true)
     * @Assert\Url
     * @Assert\Unique
     */
    private $url;

    private $map;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function fetchDataRest(string $path, string $paramsGet = ''): ?array
    {
        $client = HttpClient::create();
        try {
            $response = $client->request("GET", $this->url . $path . $paramsGet);
            return $response->toArray();
        } catch (TransportExceptionInterface $e) {
            return null;
        }
    }
}
