/**
 *
 * @param nbRows
 * @param nbCols
 * @returns {Promise<void>}
 */
export async function my2048(nbRows, nbCols) {
    let currentTab = [];
    let score = 0;
///// process
    let oldturn = document.getElementById("gameOver");
    oldturn.innerHTML = "";
    currentTab = await createTab(nbRows, nbCols);
    currentTab = await generateCell(currentTab, 2);
    let gameBoardHtml = await insertHtml(currentTab);
    document.getElementById("games").append(gameBoardHtml);
    ///////////////////functions to create game
    /**
     *
     * @param rows
     * @param cols
     * @returns {[..,[]]} tab with good length and all value equal 0
     */
    function createTab(rows, cols) {
        let tab = []
        let line = []
        for (let r = 0; r < rows; r++) {
            for (let c = 0; c < cols; c++) {
                line[c] = 0
            }
            tab[r] = line
            line = []
        }
        return tab;
    }

    /**
     * to choose one cell of the tab and generate random val (90% chance to have 2 else 4)
     * @returns {{val: number, col: number, line: number}}
     */
    function random1() {
        let r0_ = Math.floor((Math.random() * nbRows) + 1)
        let r_0 = Math.floor((Math.random() * nbCols) + 1)
        let chance = Math.random() * 100;
        let valCell;
        if (chance < 90) {
            valCell = 2;
        } else {
            valCell = 4;
        }
        return {"line": r0_, "col": r_0, "val": valCell};

    }

    /**
     *
     * @param t {[currentTab]}
     * @param nb number of cell to generate
     * @returns {Promise<[currentTab]>}
     */
    async function generateCell(t, nb) {
        if (nb === 2) {
            let cell1 = await random1();
            let cell2 = await random1();
            while (cell2.line === cell1.line && cell2.col === cell1.col) {
                cell2 = await random1();
            }
            t[cell1["line"] - 1][cell1["col"] - 1] = cell1["val"]
            t[cell2["line"] - 1][cell2["col"] - 1] = cell2["val"]
        } else {
            let cell1 = await random1()
            t[cell1["line"] - 1][cell1["col"] - 1] = cell1["val"]
        }
        return  t;
    }
    /**
     * create the html of gameBoard and score
     * @param tab
     * @returns {HTMLDivElement}
     */
    function insertHtml(tab) {
        let newtable = document.createElement("div");
        newtable.classList.add("map", "col-xs-12");
        let r = 1;
        let c = 1;
        for (let line of tab) {
            let row = document.createElement("div");
            row.classList.add("r-" + r, "col-xs-" + nbCols);
            for (let val of line) {
                let cell = document.createElement("div");
                cell.classList.add("cell", "r" + r + "-c" + c);
                cell.innerText = val;
                if (val === 0) {
                    cell.setAttribute("style", "color: transparent;")
                }
                row.append(cell);
                c++;
            }
            newtable.append(row);
            c = 1;
            r++;
        }
        let divScore = document.createElement("div");
        divScore.classList.add("score");
        divScore.innerHTML = `<p>Score : ${score}</p>`;
        newtable.append(divScore);
        return newtable;
    }
    /////////////////////////////////////////////////

    //////functions for move
    /**
     * Process of move in currentTab return true if has move, false else
     * @param direction
     * @returns {Promise<boolean>}
     */
    async function move(direction) {
        let verifyMove = false;
        let newTabLine = [];
        let size = nbCols;
        let where = 'before';
        if (direction === "down" || direction === "up") {
            currentTab = await createColTab();
            size = nbRows;
        }
        if (direction === "left" || direction === "up") {
            where = 'after';
        }

        for (let l = 0; l < size; l++) {
            newTabLine = await removeZero(currentTab[l]);
            newTabLine = await fusion(newTabLine, direction);
            newTabLine = await insertZero(newTabLine, where, size);
            for (const [i, v] of newTabLine.entries()) {
                if (currentTab[l][i] !== v) {
                    verifyMove = true;
                }
            }
            currentTab[l] = newTabLine;
        }
        if (direction === "down" || direction === "up") {
            currentTab = await createColTab();
        }
        return verifyMove;
    }

    /**
     * transform column in line of currentTab
     * @returns {[currentTab]}
     */
    function createColTab() {

        let tabCol = [];
        let colToLine = [];
        for (let c = 0; c < nbCols; c++) {
            for (let l = 0; l < nbRows; l++) {
                colToLine.push(currentTab[l][c]);
            }
            tabCol.push(colToLine)
            colToLine = [];
        }
        return tabCol;
    }

    /**
     * remove zero of tab line
     * @param t {currentTab[]} Line in move()
     * @returns {[]}
     */
    function removeZero(t) {

        let l = t.length;
        let newT = [];
        for (let i = 0; i < l; i++) {
            if (t[i] !== 0) {
                newT.push(t[i]);
            }
        }
        return newT
    }

    /**
     * if two equals cell in direction move, remove one cell and second = value * 2
     * @param line {[]} newTabLine in move()
     * @param direction {string} left,right,up or down
     * @returns {[]}
     */

    function fusion(line, direction) {
        let newLine = [];
        if (line.length === 1) {
            return line;
        }
        if (direction === "down" || direction === "right") {
            for (let l = (line.length - 1); l >= 0; l--) {
                if (line[l] === line[l - 1]) {
                    newLine.unshift(line[l] * 2);
                    score = score + line[l]*2;
                    l--;
                }
                else {
                    newLine.unshift(line[l]);
                }
            }
        } else {
            for (let l = 0; l <= (line.length - 1); l++) {
                if (line[l] === line[l + 1]) {
                    newLine.push(line[l] * 2);
                    score = score + line[l]*2;
                    l++;
                } else {
                    newLine.push(line[l]);
                }
            }
        }
        return newLine;
    }

    /**
     * insert zero before or after value in tab for good length
     * @param line {[]} newTabLine in move()
     * @param where {string} before or after
     * @param size {number} nbRows for up and down, nbCols for left and right
     * @returns {[]}
     */
    function insertZero(line, where, size) {
        while (line.length < size) {
            if (where === 'before') {
                line.unshift(0);
            } else {
                line.push(0);
            }
        }
        return line;
    }
////////////////////////////////

    //////////////function process game
    document.addEventListener("keyup", start);

    /**
     * to catch key value and insert new gameboard after move or stop game when lose
     * @param event
     * @returns {Promise<void>}
     */
    async function start(event) {
        let map = await document.getElementsByClassName("map")[0];
        let hasMove = false;
        if (typeof map !== 'undefined') {
            await map.parentNode.removeChild(map);
            switch (event.key) {
                case "ArrowRight":
                    hasMove = await move("right");
                    break
                case "ArrowLeft":
                    hasMove = await move("left");
                    break
                case "ArrowUp":
                    hasMove = await move("up");
                    break
                case "ArrowDown":
                    hasMove = await move("down");
                    break
                default:
                    break;
            }
            if (hasMove !== false) {
                await insertNewCell(currentTab);
                document.getElementById("games").append(await insertHtml(currentTab));
                return;
            } else {
                if (await hasLose() === true) {
                    gameOver();
                } else {
                    document.getElementById("games").append(await insertHtml(currentTab));
                }
            }
        } else {
            return;
        }
    }
    /**
     * insert new val in random empty cell for each turn
     * @param oldTab
     * @returns {Promise<void>}
     */
    async function insertNewCell(oldTab) {
        let tab = []
        for (let r = 0; r < nbRows; r++) {
            for (let c = 0; c < nbCols; c++) {
                if (currentTab[r][c] === 0)
                    tab.push([r, c])
            }
        }
        if (tab.length === 0) {
            return;
        }
        let newCell = await random1();
        let choseCell = Math.floor((Math.random() * tab.length));
        oldTab[tab[choseCell][0]][tab[choseCell][1]] = newCell['val'];
        currentTab = oldTab;
    }

    /**
     * check if you can play
     * @returns {boolean}
     */
    function hasLose() {
        let lose = true;
        for (let l = 0; l < nbRows; l++) {
            for (let c = 0; c < nbCols; c++) {
                if (currentTab[l][c] === 0) {
                    lose = false;
                    break;
                } else if (c === nbCols - 1 && l !== nbRows - 1) {
                    if (currentTab[l][c] === currentTab[l + 1][c]) {
                        lose = false;
                        break;
                    }
                } else if (l === nbRows - 1) {
                    if (currentTab[l][c] === currentTab[l][c + 1]) {
                        lose = false;
                        break;
                    }
                } else {
                    if (currentTab[l][c] === currentTab[l + 1][c] || currentTab[l][c] === currentTab[l][c + 1]) {
                        lose = false;
                        break;
                    }
                }
            }
            if (lose !== true) {
                break;
            }
        }
        return lose;
    }

    /**
     * insert message when you lose with score and remove eventListener(for replay)
     */
    function gameOver() {
        let gameOver = document.getElementById("gameOver");
        if (gameOver.innerText === "") {
            let messH = document.createElement("h1");
            let messP = document.createElement("p");
            messH.textContent = "Game Over!!";
            messP.textContent = "Vous avez perdu votre score est de : "+score;
            gameOver.append(messH);
            gameOver.append(messP);
            document.removeEventListener("keyup", start);
        }
    }
}